package service

import (
	"context"
	"crm_service/config"
	"crm_service/genproto/crm_service"
	"crm_service/grpc/client"
	"crm_service/pkg/logger"
	"crm_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type JurnalService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*crm_service.UnimplementedJurnalServiceServer
}

func NewJurnalService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *JurnalService {
	return &JurnalService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *JurnalService) Create(ctx context.Context, req *crm_service.CreateJurnal) (resp *crm_service.Jurnal, err error) {

	i.log.Info("---CreateJurnal------>", logger.Any("req", req))

	pKey, err := i.strg.Jurnal().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateJurnal->Jurnal->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Jurnal().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyJurnal->Jurnal->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *JurnalService) GetByID(ctx context.Context, req *crm_service.JurnalPrimaryKey) (resp *crm_service.Jurnal, err error) {

	i.log.Info("---GetJurnalByID------>", logger.Any("req", req))

	resp, err = i.strg.Jurnal().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetJurnalByID->Jurnal->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *JurnalService) GetList(ctx context.Context, req *crm_service.GetListJurnalRequest) (resp *crm_service.GetListJurnalResponse, err error) {

	i.log.Info("---GetJurnals------>", logger.Any("req", req))

	resp, err = i.strg.Jurnal().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetJurnals->Jurnal->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *JurnalService) Update(ctx context.Context, req *crm_service.UpdateJurnal) (resp *crm_service.Jurnal, err error) {

	i.log.Info("---UpdateJurnal------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Jurnal().Update(ctx, req)
	if err != nil {
		i.log.Info("!!!UpdateJurnal--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	// fmt.Println("ok1")
	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}
	// fmt.Println("ok2")

	resp, err = i.strg.Jurnal().GetByPKey(ctx, &crm_service.JurnalPrimaryKey{Id: req.Id})
	// fmt.Println("ok3")

	if err != nil {
		i.log.Error("!!!GetJurnal->Jurnal->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}
	return resp, err
}

func (i *JurnalService) Delete(ctx context.Context, req *crm_service.JurnalPrimaryKey) (resp *crm_service.JurnalEmpty, err error) {

	i.log.Info("---DeleteJurnal------>", logger.Any("req", req))

	err = i.strg.Jurnal().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteJurnal->Jurnal->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &crm_service.JurnalEmpty{}, nil
}
